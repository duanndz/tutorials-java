package com.duanndz.bean.validation;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Created By duan.nguyen at 12/10/20 9:56 AM
 */
public class BeanValidationEx {

    public static void main(String[] args) {
        // Validator object.
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        // Defining the Bean
        User user = new User();
        user.setName("Duan Nguyen");
        user.setWorking(true);
        user.setAboutMe("name: Duan Nguyen Dinh");
        user.setAge(29);
        user.setEmail("ngdduan@gmail.com");

        Set<ConstraintViolation<User>> violations = validator.validate(user);
        System.out.println("Error Size: " + violations.size());
        for (ConstraintViolation<User> violation : violations) {
            System.out.println(violation.getMessage());
        }
    }

}

package com.duanndz.bean.validation;

import java.util.regex.Pattern;

/**
 * Created By duan.nguyen at 12/16/20 3:07 PM
 */
public class RegexExpressionEx {

    public static void main(String[] args) {
        String regNum = "202016000000034";
        String pattern = "^20\\d\\d(0[1-9]|1[012])\\d+";
        System.out.println("--------------");
        System.out.println(Pattern.matches(pattern, regNum));
    }

}

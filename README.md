# Java Tutorials
    examples of Java Technologies.
    
## Java Collections Framework

Some examples about Java Collections Framework [Java Collections](./collections/README.md)

## Java Bean Validation

Some examples about [Java Bean Validation](./bean-validation/README.md)

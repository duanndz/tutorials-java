package com.duanndz.tutorials.datatime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created By duan.nguyen at 5/5/21 11:10 AM
 */
public class SimpleDateFormatMain {

    public static void main(String[] args) throws ParseException {
        var date = "3/5/2021";
        Date parsedDate = new SimpleDateFormat("M/d/yyyy").parse(date);
        System.out.println("parsedDate: " + parsedDate.toString());
    }

}

# Java Cron Expression

## Documentations

    1. https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm
    2. https://examples.javacodegeeks.com/enterprise-java/quartz/java-quartz-with-mysql-example/
    3. https://www.baeldung.com/quartz
    4. 

##  A Cron Expressions
    
    Cron expressions are used to configure instances of CronTrigger, a subclass of org.quartz.Trigger. 
    A cron expression is a string consisting of six or seven subexpressions (fields) that describe individual details of the schedule.
    These fields, separated by white space, can contain any of the allowed values with various combinations of the allowed characters for that field. 
    Table A-1 shows the fields in the expected order.

1. Cron Expressions Allowed Fields and Values
    
    Pattern: **Seconds Minutes Hours DayOfMonth Month DayOfWeek Year**
    - Seconds: (Y) (0-59) (, - * /). example: *
    - Minutes: (Y) (0-59) (, - * /). example: *; 5,10(5th, 10th); 5-10(from 5 to 10); 2/5(each 5 minutes, start from 2).
    - Hours: (Y) (0-23) (, - * /). Example: *; (1,5); 1-5; 1/5(each 5 hours, start from 1h).
    - DayOfMonth: (Y) (1-31) (, - * ? / L W C). Example: 
    - Month: Y (0-11 or JAN-DEC) (, - * /).
    - DayOfWeek (Y) (1-7 or SUN-SAT) (, - * ? / L C #)
    - Year (N) (empty or 1970-2099) (, - * /)
    



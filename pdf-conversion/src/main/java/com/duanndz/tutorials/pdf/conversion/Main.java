package com.duanndz.tutorials.pdf.conversion;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created By duan.nguyen at 12/5/20 3:23 PM
 */
public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("------ Running ------");
        // 1. Load html file
        String html = new String(Files.readAllBytes(Paths.get("./html/index.html")));
        System.out.println(html);
        // 2. Parse html to pdf.
        generatePDFFromHTML(html);
    }

    private static void generatePDFFromHTML(String html) throws Exception {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("./html/index.pdf"));
        document.open();
        XMLWorkerHelper helper =  XMLWorkerHelper.getInstance();
        helper.getDefaultCssResolver(false);
        helper.parseXHtml(writer, document, new ByteArrayInputStream(html.getBytes()));
        document.close();
    }

}

package com.duanndz.tutorials.pdf.conversion;

import com.github.jhonnymertz.wkhtmltopdf.wrapper.Pdf;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created By duan.nguyen at 12/5/20 4:20 PM
 */
public class WkHtml2PdfEx {

    public static void main(String[] args) throws Exception {
        System.out.println("WkHtml2Pdt running...");
        // 1. Load html file
        String html = new String(Files.readAllBytes(Paths.get("./html/index.html")));
        System.out.println(html);

        // 2. Convert html to pdf
        Pdf pdf = new Pdf();
        pdf.addPageFromString(html);
        // Save the PDF
        pdf.saveAs("./html/output.pdf");
    }

}
